5/21/19
Finished touching up the main floor 3 map. 
Game is in buggy, but presentable state as an early demo.

5/22/19
Fleshed out the main floor 2 layout

5/26/19
Replaced text boxes with slices anchored to NPCs
Running into a wall now displays standing/idle frames
Normal running and idle animations now in the framework of actual movement, rather than key press
Debug keys can now be disabled before distribution
*Having trouble with environment-based palette tweaks; causes dead guards
*It may be time to clean up the script. Getting pretty messy.

06/01/19
Added slide/momentum to player's movement if running fast enough
*Major bug with this: he ignores walls while sliding.

06/05/19 - current.zip
-Refined slide timing to be a little more subtle
-Player no longer ignores walls while sliding
-Sprint speed added (activates if running long enough -- may turn this into a 'boost button' instead of having it be automatic)
-Sneak speed implemented (hold ctrl or JOYA)
-Run speed no longer automatic (hold shift or JOYX)
-Pebbles now thrown with JOYR1
-Npc activation now with JOYL1
-Keypress code and map scripts better organized
-Walk frame ticks (delay between frames) can now be altered during play
*Bug: starts player's walkabout back from initial frame - may not be an issue when more frames are drawn for different speeds
*Bug: for some reason, walk frame ticks need to be declared around teleport to map commands
*Bug: doing a "this := that" in map script is making guards 'dead'
*Bug: throwing pebbles makes you run, but only when moving south?



06/09/19
-Player can now pause the game (screen darkens with random pause message). Any key resumes.
	*Bug: Currently it will rapidly cycle pause/unpause
-Guards' "Hey! You There!" text now anchored to guard
-Pebbles now run captured script if pebble crosses into guard sight.
-Pebble 'hit something' animation in place. (A bit too subtle to notice right now) 
-Refinements made to movement; run speed now instant on command, smaller slide added to walk, no slide planned for sneak speed
-NPC activation now on JOYB
-System now in place for palette tweaks based on map (for atmosphere). For now this is done with passed arguments. Global variable use for this is buggy
-Checkpoints in stable condition. For now they respawn every map reload. 
	*BUG: fairly high probability of incorrect loading of next map. 



06/17/19
-Pause bug fixed. Activates on new keypress only
-Pebbles bounce off walls now
-Locale variable now functional - palette tweaks no longer controlled by passed arguments
-Player sound vol now depends on player speed (will eventually create new sound vol for player)
-Added new npc-based path system
	*Bugs with this: guards don't stop on pause until reaching next node.
	Odd movement on sound and visibility triggers
	At the moment only one path supported. A more automatic system planned, to remove such limitations
-Responsive slice-based camera added
	*Bug: not getting sound in Test Game, even with all defaults up. Curious.
-Rearranged globals so as not to step on guard globals.	
	*This has somewhat lessened certain issues, but unfortunately this has not completely eradicated random, strange guard behavior upon map load. This happens more often leaving VR3shed back to VR3 map
-Checkpoint system finalized


06/24/19
-Universalized direct-to-player string handling for automatic positioning, fade in, and fade out
-Finished floor plan for first floor. Basement left to do and all floor plans complete
	*Node path project on hold for the time being - sticking with scripted movements for now
-Appear to have fixed strange guard behavior on VR3 map load - a "wait (5)" had been placed in map script
-Added shadow to player sprite (temporarily by way of npc, will be changed to slice later)
-Added multi-purpose npc map trigger system based on door script. These trigger npcs will display text/alter the map/trigger absolutely anything your heart desires. In later phases, these will support cursor selection for adventure game style "look" and "interact" features. For now they will be used to help the player through the demo with instructions and tips
-Temporary fix for pebblestash displaying both messages in pick up pebble with a 'wait for key'
-Duration of direct-to-player messages/strings now stay on screen longer per string length

07/15/19
-In response to common feedback a design decision was made: removed RNG element to guard sound response. Vigilance no longer affects sound response. Alert value 0-4 governs the nature of the response:
	*alert (0) = business as usual, alert(1) = look in direction of sound, alert(2) = walk to sound, alert (3) = walk to player x,y, alert(4) = *run* to player x,y
	To balance predictability now introduced, discernably higher ranks/classes of guards will behave differently and/or more aggressively.
-Added dialogue for each alert level
-Guards now store and return to original direction after returning to original path (before they would walk back to original position, but not original orientation)
-Converted wipes to slices
	*This was bloody difficult and I deserve a medal
-Converted player shadow to slices
	*bugged out right now - shows up in strange areas off screen or strange parts of the tree
-Fixed no sound during test game (fresh install / sdl2)
-Built 'roguelike' random room mini-game
	*incomplete - needs scoring system, and working wander path
-New graphics: Big door opens (requires scripting), pebble stash sack, hall arches
-Bug fixed: Guard dialogue now follows specific guard speaking, no longer defaults to 0 copy


100519
*Script cleanup began 092919. Some changes made by tmc, including a run through the indentation tool. 
*destroy slice script removed, as it cannot fulfill its intended function
*cursor select sound set, instead of scripting it. Initial sound made by menu script kept in as cue for player
*Changed foot offset for the player's graphics to make more sense. This requires a new wallmapping method going forward. In the process of converting existing wallmaps
*Player now uses npc shadow () instead of redundant use of slice collection. Changes made to that script to allow for custom shadow size, and old script deleted. arg "y" plac
*hero movement script redesigned. Speed and sliding systems have been changed
*camera peek modified to stay in direction until toggled off (not yet a polished feature. needs to keep camera fixed on peekdist)
	$bug: New bug appeared. player briefly facing door as he enters new map
*wander path debugged and improved
*deleting math.hss from my project folder, as it seems to have no use. If required later, search BL backups older than 100519

100719
	$bug: key:v debug to see npc visibility stalls game with infinite loop

100819
*PEEK camera further refined
	-Keeps position once extended
	-Toggles off if player presses tab while facing last dir or reaching peak run speed
	-Else redirects peek direction to new direction
*added hud slices for peeking
*Some changes to the IF block structures in hero script per TMC's request

101219
*guard movement script reorganized. Not yet functional to previous level but close
*create player script written for uniform shadow placement. Door, respawn, reset map scripts updated to "create player" and simply set new position for player
	$bug: if orientation of player is altered during door use, wipe/door script do not complete, and stalls the game, sometimes with the wipeblock being stuck, usually not allowing for ESC menu. 
*created small script attached to default npcs which adjusts npc positions to counteract side effects from the foot offset solution. This is mostly for invisible 'set piece' npcs whose positions need to match the tiles better 

101319
*more work done on player movement script. 
	*Gradual speed decrease re-introduced.
	*Double tap sprint
	$: double tap sprint only working for keyboard. Gamepad keyhandler block works as well as the keyboard version, but no change in player behavior

102019
*Guard script undergoing rewrite.
	-Restructured code
	-Patched realignment/pathfinding bug (... !!!!!!!!) May not be the most sophisticated solution, but appears to work, even in tight spaces.
	-Turned "chase player" into an individualized global "chasing"
	-Guards now gang up, but only if in a certain range of the chase, rather than map-wide
	-Alert levels introduced for hearing player. No longer an instant chase.
	-Alert decisions splits off in branches. First determined if instigator was something seen, or heard, then distinguishes between exactly what it saw or heard before deciding what to do

102819
* Guard code rewrite continues
	- Improved realignment introduced by tmc
	- Alert stimuli hierarchy introduced
* Player movement now creates sound in intervals with a timer. Guards track the last heard sound rather than the player's current coords

103019
* fps increased to 30
* cooldown now triggers separate alert behaviors
* The fundamental relationship between guard ai and player capabilities required relative adjustments to player's speed settings and speed control
* SPRINTING:
	- player sprint now accomplished by pressing shift and arrow simultaneously (timer created to give player a brief window so that inputs don't have to occur in exactly the same tick)
	- sprint is now treated like a brief boost, but only if you execute sprint correctly

110219
* added player hide
	- if sneaking and staying perfectly still, player can only be seen at a very short distance
	- visual cue introduced for this
* Fixed initial menus

110319
* added background ellipse to appearance selection npc. Not digging it.
* fixed pause screen
	- pause stops guards in their tracks - still does not affect pathfinding, however
*Icon for hunger created, but hidden

110719
* Sprinting now accomplished with holding shift to power it up first. Sprinting goes indefinitely, but is delicate to maintain. Running into any walls, or any moment without a direction input will kill it. This may be ideal for this feature.
	-bug with this? After an extended period, sprinting seems to cancel on its own

110819
* eliminated all error messages except npc activation on deleted npcs. However, this is an error that will not show in the distributable
	-engine bug: though you do not see an error, there is a slight pause in the gameplay 
* for now, all doors will be set to WIDE. Automatic alignment with these + foot offset = bad door activation
* slide fixed. (target speed system was depriving character of floor slide)
* Organized keyhandler and updated joystick controls to match keyboard behaviors

110919
* Pebble number now turns red when pebble sack maxed out to reduce confusion (informed by tester feedback) 
* Slice pebble script begun, not yet implemented

111419
* Sprint added back in with new concept. When moving, energy builds for sprint. Once timer has counted down player can sprint when they re-press shift. Sprint lasts for a period of time before reducing to run speed. *Many* small adjustments were necessary for this complicated series of conditions.
	$bug?: new keypress not working with joy buttons
* indicator slice added to let player know he can sprint. Animated variation to player shadow.
* 'directions pressed' script added because `want dir` checks do no good outside player script, as player script always sets want dir to -1 at the end
* Heavy breathing SFX added to help indicate to player he is sprinting. Other breathing SFX added for when player either stops mid-sprint, or sprint duration has ended
* Music fades to a diminished volume when paused
* New keypress script for while game is paused. This fixes an issue where the same pause key on either keyboard or joystick could not also UNpause, because the normal keypress script has been suspended

111519
* Return to Position: When guard deviates from path, he now returns to that position and orientation. 	-Return to Position works well for guards who are originally set to simply stand somewhere, 	but Return to Path will have to be created for guards on paths. In the interrim, guards are 		set to wander after returning to last position.
* Rearranged some globals to give player 50 more 'slots'
* Began adjusting some walls in the editor for new foot offset
* Set framework for increasing hunger during strenuous activities such as sprinting or hiding

111619
* Hunger implemented
	-As of now, max hunger is 299
	-HUD slice installed for this. Eventually slice will change frame instead of showing a number
	-Activated checkpoint now stores current hunger value at time of activation
	-Hunger powerup added. Three levels: bad food, good food, mana food. For now acts like checkpoint in that it will remove all other food of its kind on the map. Will have to set up a table in order for there to be more than one source of food in one map	
	-Hunger-related code in misc.hss moved to items.hss. As of now, food is an instant "powerup" object, but it's intended to let the player store bits of food in inventory as an item. 

111819
* Fixed keyhandler issues with keyboard - joystick not yet up to speed
* Implemented "setpiece" npcs for one-space objects to take advantage of relative Y layering
	- Most possible pillar combinations have now been added to walkabout selection
	- Still working with the foot offset vs. the orientation of the walkabout graphics

111919
* Created an instruction manual and keyboard/gamepad diagrams

112019
* Hide refined.
	-must hold sneak button and be still for one second
	-reverse dissolving 'hide sprite' added
* Npcs will now only play sound effects (for instance their footsteps) if within pixel range of player

112319
* Slide fixed
* Did much work on the starting menus
	-Player can now rotate sample player sprite
	-Added title
	-Displays version and date (month/year)
* Reformation of tilesets. Before offset, splitting graphics between two tiles helped player look centered. Now that player is adjusted, these graphics are being changed to inhabit one tile.
* Maps are undergoing wallmap/tilemap edits
* Improved player walkabout

112419
* Guard speed now controlled with the 'myspeed' variable, and updated with only 1 or 2 `set npc speed`s
* Fixed bug where player could slide during pause/reset menus
$bug - sometimes entering a room with no clip activated can deaden all guards - point of focus: VR3 map

112719
More emphasis on graphics and mapping
* Created fresh VR3 map which seems to have undone whatever voodoo that map had.
* Converted a few more maptiles to setpieces
	- Big tree, information npc, fences
* Rocky path tiles expanded to allow for corners / T-junctions / etc.
* VR levels updated and revised for current game mechanics
* TR levels updated and revised for current game mechanics
* Joystick controls (mostly) restored
	- code is not optimally written, (redundant blocks to keyboard), but functional
	$bug - having difficulty getting `new keypress` to work
	$bug - button 7 does not close reset menu after opening it
* Game messages / strings added for eating

1128/29: break

113019
* Hedging tiles added to improve edges in TR1
* SFX added for intiating / retracting peek cam

120119
ALPHA 1.1 RELEASE

1202-1208: break

120919
* Brightened all palette changes. Game was a little too dark in general
* Updated HUD
	-Removed sidebars in favor of a top bar, taking advantage of new opacity feature
	-Cloaking (player hide) now focuses player view to limited range
* Sprinting extends camera a ways in player direction
	$bug: selecting level in game start menu with CTRL instead of ENTER triggers player hide during new game spawn.

121419
* Trying new screen resolution now that I have removed side bars. Taking note of previous resolution: 400x272. New resolution in 4:3, 384x288
* Game messages no longer re-display if already on screen. Requested messages are compared and copied to a second set of strings (never displayed) to keep track

121519
* Started implementation of walkabout animation testing. Only looping animation supported (edit npcs of, and teleport to map 47)
* Set up a YouTube channel and have worked out video capturing so that sound and music can be showcased

122219
* some various small adjustments
* code error fixed in guard ai
* timeout timer implemented: long period of no inputs auto-pauses the game
* peek cam distance extended
* cam looks slightly in player direction while moving slowly
* working on guard footstep sounds while in player in proximity
* made adjustments to guard ai alert and cooldown activities
* moved several scripts to the "misc.hss" which seem to require little to no further development at this time
* pause screen now fades to reddened palette, instead of abrupt change. Getting it to fade back is proving weird, so for now the palette immediately returns to normal.
* Pause text now routed through `init string`

122419
* guard footstep sound code implemented. As long as there are 1 or more guards in the prescribed area sound 12 plays. (using a slice for now, sorry! we can change it if you have a better solution). 
	$bug: guards play this sound whether moving or not
* pause screen now pauses all sounds and resumes them on game resume. game resumes on any key now
* added sound to reset menu "think you can sneak away?"
$bug: some path behaviors are being remembered by guards after reset map. They have become sentient O_O
*added sfx handler to play up to three sounds in succession with one command `play sounds (sound1, sound2, sound3). The third sound can be looped
* added different footstep sound effects for each player speed

122819
* Increased wait time now between maps in door script. Adds some weight to the transition.
* Some more work on the guard AI to eliminate some strange behavior
* Smoothed out the transition from caught to respawn; now free of glitchy images and camera movement
* Checkpoint changes palette when player is seen, rather than disappears
* music fade triggers now in script call format for convenience
* camera bug fixed where pausing let camera keep sliding.
* displaying pebble number string in hud converted to call in main loop so that player hide can hide the string, but have the string return

011220
* Sprinting for gamepad: tap sneak button while running; for keyboard: tap space bar
* HUD refined - transparent boxes now frame icons and game messages
* Option menu added for choice between keyboard or gamepad. For now this only affects what controls instructions the player gets
* Game message colors brightened for easier reading
* Workaround implemented for npc activation error when deleting npcs. Npc repositioned to 0,0 and main loop constantly deletes any npcs in that position
* Guards who 'use' doors in their path script are now sent to 1,0 where a wallmap square holds them in place

011320
* Transparent text box now fits the width of the longest string, as well as the height of the overall `curstring` message
* Adjustments made to menu / text colors
